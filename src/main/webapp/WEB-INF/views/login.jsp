<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href='<c:url value="/resources/css/login.css"/>' />
</head>
<body>
	<div class="login-page">
		<div class="form">
			<form class="login-form" method="post">
				<input type="text" placeholder="email" name="username" /> <input
					type="password" placeholder="password" name="psw" />
				<button>login</button>
				<p class="message">
					Not registered? <a href="register">Create an account</a>
				</p>
			</form>
		</div>
	</div>
	<%-- <script src='<c:url value="/resources/js/login.js"/>'></script> --%>
</body>
</html>