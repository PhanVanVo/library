<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href='<c:url value="/resources/css/detail.css"/>' />
<meta charset="UTF-8">
<link rel="shortcut icon" type="image/png"
	href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png"
	id="favicon"
	data-original-href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png" />
<title>${bookdetail.getBookName()}</title>
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

</head>
<body>
	<font size="30">
	<c:if test="${user.getRooller() eq 1}">
    <a href="http://localhost:8080/Library/CRUDBook">
    	 ${user.getFullname()}
     </a>
     <a href="logout"  class="pull-right"> logout </a>
    </c:if>
    	<c:if test="${user.getRooller() eq 0}">
    <a href="http://localhost:8080/Library/CRUDBook">
    	 ${user.getFullname()}
     </a>
     <a href="logout"  class="pull-right"> logout </a>
    </c:if>
	</font>
	
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
		<div class="container">
			<a class="navbar-brand" href="/Library/member">HOME</a>
			
		</div>
	</nav>
	<hr>
	<!-- 	======================== -->
	<div class="container-fluid">
		<div class="content-wrapper">
			<div class="item-container">
				<div class="container">
					<div class="col-md-12">
						<div class="product col-md-3 service-image-left"
							style="width: 41%;">
							<a
								href='<c:url value ="/resources/bookFile/${bookdetail.getContent()}"/>'
								target="_blank"> <img id="item-display"
								src='<c:url value="/resources/img/${bookdetail.getImg()}"/>'
								width="100%" height="100%" style="border-radius: 9px;">
							</a>
						</div>
					</div>

					<div class="col-md-7">
						<div class="product-title">${bookdetail.getBookName()}</div>
						<input id="idbook" value="${bookdetail.getIdBook()}" type="hidden">
						<div class="container ngoisao">
				    		<input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="0.1" value="${start}">
   						</div>
						<hr>
						<div class="product-price">Author:
							${bookdetail.getAuthors()}</div>
						<div class="product-stock">Category:
							${bookdetail.getCategory()}</div>
						<hr>
						<div class="btn-group cart">
							<button type="button" class="btn btn-success">
								<a href='<c:url value ="/resources/bookFile/${bookdetail.getContent()}"/>' target="_blank">Read Online</a>
							</button>
						</div>
						<div class="btn-group wishlist">
							<button type="button" class="btn btn-danger">
								<a href='<c:url value ="/resources/bookFile/${bookdetail.getContent()}"/>'
									download>Download</a>
							</button>
						</div>
					</div>
				</div>
			</div>
			<hr>
						<!-- -------------------------------------------------------- -->
			<div class="row bootstrap snippets">
    <div class="col-md-6 col-md-offset-2 col-sm-12">
        <div class="comment-wrapper">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Comment panel
                </div>
                <div class="panel-body" id = "ScollBar">
              
                    <div class="clearfix"></div>
                    <hr>
                    <ul class="media-list">
                  
                         <c:forEach var="cmt" items="${listcmt}">
                        <li class="media">
                          <div class="container">
				    		<input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="0.1" value="${cmt.getRankStart()}">
   						</div>
   							<c:if test="${cmt.getBookUser().getIdBookUer() eq user.getIdBookUer()}">
                            <a href="#" class="pull-left">
                                <img src="https://bootdey.com/img/Content/user_3.jpg" alt="" class="img-circle">
                            </a>
                            </c:if>
                           
                            <c:if test="${cmt.getBookUser().getIdBookUer() ne user.getIdBookUer()}">
                            <a href="#" class="pull-left">
                                <img src="https://bootdey.com/img/Content/user_1.jpg" alt="" class="img-circle">
                            </a>
                            </c:if>
                            


                            <div class="media-body">
                                <span class="text-muted pull-right">
                                    <small class="text-muted">${cmt.getTime()}</small>
                                </span>
                                
                                <strong class="text-success">${cmt.getBookUser().getFullname()}</strong>
                                <p>
                                    ${cmt.getContent()}
                                     
                                </p>
                            </div>
                        </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
			<!-- --------------------------------------------------------- -->
</div>
	
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="well well-sm">
							<div class="row" id="post-review-box">
								<div class="col-md-12">
									<form accept-charset="UTF-8" action="" method="post">
										<input id="ratings-hidden" name="rating" type="hidden">
										<textarea class="form-control animated" cols="50"
											id="new-review" name="textarea" name="comment"
											placeholder="Enter your review here..." rows="5" autofocus></textarea>
										<div class="text-right">
											<div class="stars starrr" data-rating="0"></div>
											<button class="btn btn-success btn-lg" type="submit">commnet</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>

<!-- ------------------------------------------------------------------ -->
		

	<script src='<c:url value="/resources/js/detail.js"/>'></script>
	<script src='<c:url value="/resources/js/comment.js"/>'></script>
</body>
</html>