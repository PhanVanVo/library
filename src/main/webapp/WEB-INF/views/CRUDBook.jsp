<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" type="image/png"
	href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png"
	id="favicon"
	data-original-href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png" />
<script type="text/javascript">
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});
</script>

<link rel="stylesheet" href='<c:url value="/resources/css/upfile.css"/>' />
<link rel="stylesheet"
	href='<c:url value="/resources/css/upfile1.css"/>' />
<meta charset="UTF-8">
<title>ReadBook</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<link rel="stylesheet" href='<c:url value="/resources/css/CRUD.css"/>' />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
<!-- remove this if you use Modernizr -->
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
	<font size="30"> 	
	<a href="/Library/member"> Home</a> 
	<a href="/Library/CRUDBook"> ${user.getFullname()}</a>
     <a  class="pull-right" href="logout"> logout </a>
		
		
	
	</font>
	<hr>
	<!--   ---------------------------------------------------- -->
	<div class="container">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-5">
						<h2>
						User <b>Management</b>
							
						</h2>
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Date Created</th>
						<th>Read</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="LBooks" items="${listBook}">
						<tr idbook="${LBooks.getIdBook()}"
							idStatus="${LBooks.getStatus()}">
							<td>${LBooks.getIdBook()}</td>
							<td><a href="#"><img
									src="resources/img/${LBooks.getImg()}" class="avatar"
									alt="Avatar"> ${LBooks.getBookName()}</a></td>
							<td>${LBooks.getCreateAt()}</td>
							<td><a href="resources/bookFile/${LBooks.getContent()}"
								target="_blank"> View</a></td>
							<c:if test="${LBooks.getStatus() eq 1}">
								<td><span class="status text-success">&bull; </span> show <a
									href="/Library/CRUDBook" class="ChangeStatus">Change</a></td>
							</c:if>
							<c:if test="${LBooks.getStatus() eq 0}">
								<td><span class="status text-danger">&bull;</span> hide <a
									href="/Library/CRUDBook" class="ChangeStatus">Change</a></td>

							</c:if>
							<td>
							<a href="/Library/CRUDBook" class="deleteBook" title="Delete"
								data-toggle="tooltip"><i class="material-icons">&#xE5C9;</i></a>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
			<div class="clearfix">
				<div class="hint-text">
					Showing <b>5</b> out of <b>25</b> entries
				</div>
				<ul class="pagination">
					<li class="page-item disabled"><a href="#">Previous</a></li>
					<li class="page-item"><a href="#" class="page-link">1</a></li>
					<li class="page-item"><a href="#" class="page-link">2</a></li>
					<li class="page-item active"><a href="#" class="page-link">3</a></li>
					<li class="page-item"><a href="#" class="page-link">4</a></li>
					<li class="page-item"><a href="#" class="page-link">5</a></li>
					<li class="page-item"><a href="#" class="page-link">Next</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- ----------------------------------------------------------------- -->

	<button class="open-button" onclick="openForm()">Add Book</button>
  <button  class="open-buttonEdit" data-toggle="modal" data-target="#myModal">
    Open modal
  </button>
	<div class="chat-popup" id="myForm">
		<form method="post" class="form-container"
			action="/Library/CRUDBook/createBook" enctype="multipart/form-data"
			modelAttribute="book">
			<p class="addBook">Add Book</p>
			<textarea placeholder="authors" name="authors" id="authors"></textarea>
			<textarea placeholder="Book's Name" name="BookN" id="BookN"></textarea>
			<label>category: <input list="category" name="category"
				id="category" /></label> <select id="mySelect" onchange="chooseValue()">
				<c:forEach var="LBooks" items="${listBook}">
					<option value="${LBooks.getCategory()}">${LBooks.getCategory()}</option>
				</c:forEach>
			</select>
			<div class="container1">
				<div class="content">
					<div class="box">
						<input type="file" name="avatar" id="file-6"
							class="inputfile  inputfile-5" accept="image/*"
							data-multiple-caption="{count} files selected" /> <label
							for="file-6"><figure>
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
									viewBox="0 0 20 17">
									<path
										d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
							</figure> <span></span></label>
					</div>

					<div class="box">
						<input type="file" name="file" id="file-7"
							class="inputfile inputfile-6" accept=".txt, .pdf, .doc, .epub"
							data-multiple-caption="{count} files selected" /> <label
							for="file-7"><span></span> <strong><svg
									xmlns="http://www.w3.org/2000/svg" width="20" height="17"
									viewBox="0 0 20 17">
									<path
										d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
								Choose a file&hellip;</strong></label>
					</div>
				</div>
			</div>

			<button type="submit" class="btn" id="CBook" value="CRUDBook">
				Add</button>
			<button type="button" class="btn cancel" onclick="closeForm()">close</button>
		</form>
	</div>
<!-- =========================================================== -->

<div class="container">
  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">File Log</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
       <div class="container">
        <table class="table">
    <thead>
      <tr>
        <th>Time</th>
        <th>Event</th>
        <th>DESC</th>
         <th>User</th>
      </tr>
    </thead>
    <tbody>
    <c:forEach var="Logs" items="${listLogs}">
      <tr>
        <td>${Logs.getTimeLine()}</td>
        <td>${Logs.getEvent()}</td>
        <td>${Logs.getDescription()}</td>
        <td>${Logs.getBookUser().getFullname()}</td>
      </tr>
	</c:forEach>
    </tbody>
  </table>
     
     	  
        </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>  
</div>

<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

</script>
	<script src='<c:url value="/resources/js/custom-file-input.js"/>'></script>
	<script src='<c:url value="/resources/js/xuli.js"/>'></script>
	<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

</script>
	<script>
function chooseValue() {
  var x = document.getElementById("mySelect").value;
  document.getElementById("category").value =  x;
}
</script>
	<!--   -------------------------------------------------------------------------- -->

  
</body>
</html>