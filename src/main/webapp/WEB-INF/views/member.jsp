<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href='<c:url value="/resources/css/member.css"/>' />
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">

<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="shortcut icon" type="image/png"
	href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png"
	id="favicon"
	data-original-href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png" />
<title>Read Book online</title>
</head>
<body>

	<font size="30">
	<c:if test="${user.getRooller() eq 1}">
    <a href="http://localhost:8080/Library/CRUDBook">
    	 ${user.getFullname()}
     </a>
     <a href="logout"  class="pull-right"> logout </a>
    </c:if>
    	<c:if test="${user.getRooller() eq 0}">
    <a href="http://localhost:8080/Library/CRUDBook">
    	 ${user.getFullname()}
     </a>
     <a href="logout"  class="pull-right"> logout </a>
    </c:if>
	</font>
	<hr>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
		<div class="container">
			<a class="navbar-brand" href="/Library/member">HOME</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarsExampleDefault"
				aria-controls="navbarsExampleDefault" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end"
				id="navbarsExampleDefault">
				<ul class="navbar-nav m-auto">
					<li class="nav-item"><a class="nav-link" id="programming"
						href="#">programming</a></li>
					<li class="nav-item"><a href="#" class="nav-link"
						id="psychology">psychology</a></li>
					<li class="nav-item"><a href="#" class="nav-link"
						id="biological">biological</a></li>
					<li class="nav-item"><a href="#" class="nav-link"
						id="textbook">textbook</a></li>
					<li class="nav-item"><a href="#" class="nav-link"
						id="literary">literary</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="container">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2">

					<div class="input-group">
						<div class="input-group-btn search-panel">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown">
								<span id="search_concept">Filter by</span> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#0">Book Name</a></li>
								<li><a href="#1">Authors</a></li>
								<li><a href="#2">Category</a></li>
							</ul>
						</div>
						<input type="hidden" name="search_param" value="0"
							id="search_param"> <input type="text"
							class="form-control" id="valueString" name="searchText"
							placeholder="Search term..."> <span
							class="input-group-btn">
							<button id="search" class="btn btn-default" type="submit">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</span>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="row showsach">
					<c:forEach var="LBooks" items="${listBook}">
						<c:if test="${LBooks.getStatus() eq 1}">
							<div
								class="col-12 col-md-6 col-lg-4 showBook ${LBooks.getIdBook()}"
								idbook="${LBooks.getIdBook()}">
								<div class="card">
									<a href="member/detail?id=${LBooks.getIdBook()}"
										title="View Product"> <img class="card-img-top"
										src="resources/img/${LBooks.getImg()}" alt="Card image cap"></a>
									<div class="card-body">
										<h4 class="card-title">
											<a href="member/detail?id=${LBooks.getIdBook()}"
												title="View Product">${LBooks.getBookName()}</a>
										</h4>
										<p class="card-text">Author: ${LBooks.getAuthors()}</p>
										<p class="card-text">Category: ${LBooks.getCategory()}</p>
										<time style="font-size: 12px;">${LBooks.getCreateAt()}</time>
										<div class="row">
											<div class="col">

												<p class="btn btn-danger btn-block downbook">
													<a href="resources/bookFile/${LBooks.getContent()}"
														download>Download</a>
												</p>
											</div>
											<input type="hidden" name="downbook"
												value="resources/bookFile/${LBooks.getContent()}"
												id="downbook">
											<div class="col">
												<a href="resources/bookFile/${LBooks.getContent()}"
													class="btn btn-success btn-block downLbook" target="_blank">Read
													Online</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:if>
					</c:forEach>
					<div class="col-12">
						<nav aria-label="...">
							<ul class="pagination">
								<li class="page-item disabled"><a class="page-link"
									href="#" tabindex="-1">Previous</a></li>
								<li class="page-item"><a class="page-link" href="#">1</a></li>
								<li class="page-item active"><a class="page-link" href="#">2
										<span class="sr-only">(current)</span>
								</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">Next</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- Footer -->
	<footer class="text-light">
		<div class="container"></div>
	</footer>
	<script src='<c:url value="/resources/js/mem.js"/>'></script>
</body>
</html>