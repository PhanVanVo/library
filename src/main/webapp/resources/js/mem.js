$(document).ready(function(e){
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var param = $(this).attr("href").replace("#","");
		var concept = $(this).text();
		$('.search-panel span#search_concept').text(concept);
		$('.input-group #search_param').val(param);
	});
});

$(document).ready(function(){
// sort and search for books
/*====================================*/	
		$("#programming").click(function(){
			var idloaisp = 0;
			$.ajax({
				url:"/Library/member/typebook",
			    type:"GET",
			    contentType: 'application/json',
			    dataType: "json",
			    data:{
			    	maloaisp:idloaisp,
			    },
			    error : function(){
					alert('nope not yet');
				},
				success: function(value){
					//console.log(value);
					 var divsToHide = document.getElementsByClassName("showBook");
				        $('.showBook').css('display', 'none')
					 $.each(value, function(index, sp) {
			            console.log(sp); 
						
					        
			            var str='<div class="col-12 col-md-6 col-lg-4 showBook">'
							   +'<div class="card">'
							   +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'
						          +'<img class="card-img-top" '
									+'src="resources/img/'+sp.img+'" alt="Card image cap">'
									+'</a>'
						            +'<div class="card-body">'
						              +'<h4 class="card-title">'
						              +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'+sp.bookName+'</a></h4>'
						              +'<p class="card-text">Author: '+sp.authors+'</p>'
						              +'<p class="card-text" >Category: <span style="font-size: 18px; color: green">'+sp.category+'</span></p>'
						              +'<time style="font-size: 12px;">'+sp.createAt+'</time>'
								+'<div class="row">'
								+'<div class="col">'
								+'<p class="btn btn-danger btn-block"><a  href="resources/bookFile/'+sp.content+'"download>Download</a></p>'
								+'</div>'
								+'<input type="hidden" name="downbook" value="resources/bookFile/'+sp.content+'">'
								+'<div class="col">'
									+'<a href="resources/bookFile/'+sp.content+'" class="btn btn-success btn-block downLbook">Read Online</a>'
								+'</div>'
							+'</div>'
						            +'</div>'
						       +'</div>'
						      +'</div>'
						     
					     
			             $(".showsach").prepend(str);
						  $("a").removeClass("active");
						 var element = document.getElementById("programming");
						 element.classList.add("active");
						 
			         });	
					 
				}
			})
		})
/*==========================================================================*/	
		$("#psychology").click(function(){
			var idloaisp = 1;
			
			$.ajax({
				url:"/Library/member/typebook",
			    type:"GET",
			    contentType: 'application/json',
			    dataType: "json",
			    data:{
			    	maloaisp:idloaisp,
			    },
			    error : function(){
					alert('nope not yet');
				},
				success: function(value){
					
					//console.log(value);
					 var divsToHide = document.getElementsByClassName("showBook");
				        $('.showBook').css('display', 'none')
					 $.each(value, function(index, sp) {
			            console.log(sp); 
						
					        
			            var str='<div class="col-12 col-md-6 col-lg-4 showBook">'
							   +'<div class="card">'
						          +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'
						          +'<img class="card-img-top"'
									+'src="resources/img/'+sp.img+'" alt="Card image cap">'
									+'</a>'
						            +'<div class="card-body">'
						              +'<h4 class="card-title">'
						              +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'+sp.bookName+'</a></h4>'
						              +'<p class="card-text">Author: '+sp.authors+'</p>'
						              +'<p class="card-text" >Category: <span style="font-size: 18px; color: green">'+sp.category+'</span></p>'
						              +'<time style="font-size: 12px;">'+sp.createAt+'</time>'
								+'<div class="row">'
								+'<div class="col">'
								+'<p class="btn btn-danger btn-block"><a  href="resources/bookFile/'+sp.content+'"download>Download</a></p>'
								+'</div>'
								+'<input type="hidden" name="downbook" value="resources/bookFile/'+sp.content+'">'
								+'<div class="col">'
									+'<a href="resources/bookFile/'+sp.content+'" class="btn btn-success btn-block downLbook">Read Online</a>'
								+'</div>'
							+'</div>'
						            +'</div>'
						       +'</div>'
						      +'</div>'
						     
					   
			             $(".showsach").prepend(str);	
						 $("a").removeClass("active");
						 var element = document.getElementById("psychology");
						 element.classList.add("active");
			            //to print name of employee
			         });	
					 
				}
			})
		})
/*==========================================================================*/		
		$("#biological").click(function(){
			var idloaisp = 2;
			$.ajax({
				url:"/Library/member/typebook",
			    type:"GET",
			    contentType: 'application/json',
			    dataType: "json",
			    data:{
			    	maloaisp:idloaisp,
			    },
			    error : function(){
					alert('nope not yet');
				},
				success: function(value){
					//console.log(value);
					 var divsToHide = document.getElementsByClassName("showBook");
				        $('.showBook').css('display', 'none')
					 $.each(value, function(index, sp) {
			            console.log(sp); 
			            var str='<div class="col-12 col-md-6 col-lg-4 showBook">'
							   +'<div class="card">'
						          +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'
						          +'<img class="card-img-top" '
						          +'src="resources/img/'+sp.img+'" alt="Card image cap">'
									+'</a>'
						            +'<div class="card-body">'
						              +'<h4 class="card-title">'
						              +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'+sp.bookName+'</a></h4>'
						              +'<p class="card-text">Author: '+sp.authors+'</p>'
						              +'<p class="card-text" >Category: <span style="font-size: 18px; color: green">'+sp.category+'</span></p>'
						              +'<time style="font-size: 12px;">'+sp.createAt+'</time>'
								+'<div class="row">'
								+'<div class="col">'
								+'<p class="btn btn-danger btn-block"><a  href="resources/bookFile/'+sp.content+'"download>Download</a></p>'
								+'</div>'
								+'<input type="hidden" name="downbook" value="resources/bookFile/'+sp.content+'">'
								+'<div class="col">'
									+'<a href="resources/bookFile/'+sp.content+'" class="btn btn-success btn-block downLbook">Read Online</a>'
								+'</div>'
							+'</div>'
						            +'</div>'
						       +'</div>'
						      +'</div>'
						     
					   
			             $(".showsach").prepend(str);	
						 $("a").removeClass("active");
						 var element = document.getElementById("biological");
						 element.classList.add("active");
			            //to print name of employee
			         });	
					 
				}
			})
		})
/*==========================================================================*/	
				$("#search").click(function(){
			var valueText = $("#valueString").val().trim();
			var valueIndex = $("#search_param").val();
			
			$.ajax({
				url:"/Library/member/search",
			    type:"GET",
			    contentType: 'application/json',
			    dataType: "json",
			    data:{
			    	index:valueIndex,
			    	value: valueText,
			    },
				error : function(){
					alert('not found');
				},
				success: function(value){
					//console.log(value);
					 var divsToHide = document.getElementsByClassName("showBook");
				        $('.showBook').css('display', 'none')
					 $.each(value, function(index, sp) {
			            console.log(sp); 
						
					        
						 var str='<div class="col-12 col-md-6 col-lg-4 showBook">'
						   +'<div class="card">'
					          +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'
					          +'<img class="card-img-top" '
					          +'src="resources/img/'+sp.img+'" alt="Card image cap">'
								+'</a>'
					            +'<div class="card-body">'
					              +'<h4 class="card-title">'
					              +'<a href="member/detail?id='+sp.idBook+'" title="View Product">'+sp.bookName+'</a></h4>'
					              +'<p class="card-text">Author: '+sp.authors+'</p>'
					              +'<p class="card-text" >Category: <span style="font-size: 18px; color: green">'+sp.category+'</span></p>'
					              +'<time style="font-size: 12px;">'+sp.createAt+'</time>'
							+'<div class="row">'
							+'<div class="col">'
							+'<p class="btn btn-danger btn-block"><a  href="resources/bookFile/'+sp.content+'"download>Download</a></p>'
							+'</div>'
							+'<input type="hidden" name="downbook" value="resources/bookFile/'+sp.content+'">'
							+'<div class="col">'
								+'<a href="resources/bookFile/'+sp.content+'" class="btn btn-success btn-block downLbook">Read Online</a>'
							+'</div>'
						+'</div>'
					            +'</div>'
					       +'</div>'
					      +'</div>'
					     
			             $(".showsach").prepend(str);
						  $("a").removeClass("active");
						 var element = document.getElementById("programming");
						 element.classList.add("active");
						 
			         });	
					 
				}
			})
		})

/**********************************************/
});