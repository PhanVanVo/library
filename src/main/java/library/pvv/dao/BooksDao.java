package library.pvv.dao;

import java.util.List;

import library.pvv.model.Book;
import library.pvv.model.Audit_log;;

public interface BooksDao {
	List<Book> showListBooks();
	int AddBook(Book book);
	int deleteBook(int idBook);
	int updateBook(Book book);
	int changeStaus(int idBook, int status);
	int Addlog(Audit_log audit_log);
	List<Book> search(int index, String valuestrString);
	List<Book> getType(int IdType);
	Book showdetail(int id);
	List<Audit_log> showFilelog();
}
