package library.pvv.dao;

import java.util.List;

import library.pvv.model.BookUser;

public interface BookUserDao {
	List<BookUser> selectUser();
	int addUser(BookUser bookUser);
	boolean checkLogin(String username, String pass);
	BookUser showInfor(String username);

}
