package library.pvv.dao;

import java.util.List;

import library.pvv.model.comment;

public interface commentDao {
	int addcmt(int iduser, int idbook, String content, int rankStart);
	List<comment> showCMT(int idbook);
}
