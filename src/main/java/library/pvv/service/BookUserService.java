package library.pvv.service;

import library.pvv.model.BookUser;

public interface BookUserService {
	int addUser(BookUser bookUser);
	boolean checkLogin(String username, String pass);
	BookUser showInfor(String username);
}
