package library.pvv.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name = "Audit_log")
public class Audit_log {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String timeLine;
	private String event;
	private String description;
	
	//hibernate structure
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idUser")
	BookUser bookUser;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTimeLine() {
		return timeLine;
	}

	public void setTimeLine(String timeLine) {
		this.timeLine = timeLine;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public BookUser getBookUser() {
		return bookUser;
	}

	public void setBookUser(BookUser bookUser) {
		this.bookUser = bookUser;
	}

	public Audit_log(int id, String timeLine, String event, String description, BookUser bookUser) {
		super();
		this.id = id;
		this.timeLine = timeLine;
		this.event = event;
		this.description = description;
		this.bookUser = bookUser;
	}

	public Audit_log() {
		super();
	}
	
}
