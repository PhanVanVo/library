package library.pvv.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="BookUser")
public class BookUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idBookUser;
	private String fullname;
	private String username;
	private String password;
	private int rooller;
	
	public int getIdBookUer() {
		return idBookUser;
	}
	public void setIdBookUer(int idBookUser) {
		this.idBookUser = idBookUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getRooller() {
		return rooller;
	}
	public void setRooller(int rooller) {
		this.rooller = rooller;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public BookUser(int idBookUser, String username, String password, String fullname, int rooller) {
		super();
		this.idBookUser = idBookUser;
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.rooller = rooller;
	}
	public BookUser() {
		super();
	}
	@Override
	public String toString() {
		return "BookUser [idBookUser=" + idBookUser + ", fullname=" + fullname + ", username=" + username
				+ ", password=" + password + ", rooller=" + rooller + "]";
	}
	
}
