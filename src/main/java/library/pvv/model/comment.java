package library.pvv.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name ="comment")
public class comment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String content;
	private int rankStart;
	private String Time;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idUser")
	BookUser bookUser;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idBook")
	Book book;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public BookUser getBookUser() {
		return bookUser;
	}

	public void setBookUser(BookUser bookUser) {
		this.bookUser = bookUser;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	public int getRankStart() {
		return rankStart;
	}

	public void setRankStart(int rankStart) {
		this.rankStart = rankStart;
	}
	
	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public comment() {
		super();
	}

	
}
