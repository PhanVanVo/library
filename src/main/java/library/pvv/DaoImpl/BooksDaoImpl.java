package library.pvv.DaoImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import library.pvv.dao.BooksDao;
import library.pvv.model.Audit_log;
import library.pvv.model.Book;
import library.pvv.model.BookUser;

@Repository
@Transactional
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BooksDaoImpl implements BooksDao{
	
	@Autowired
	SessionFactory sessionFactory;

	//show all book and sort "order by idBook DESC"
	@Override
	public List<Book> showListBooks() {
		Session session = sessionFactory.getCurrentSession();
		List<Book> listBooks = session.createQuery("from Book order by idBook DESC").getResultList();
		return listBooks;
	}

	@Override
	public int AddBook(Book book) {
		Session session = sessionFactory.getCurrentSession();
		session.save(book);
		return 0;
	}

	@Override
	public int deleteBook(int idBook) {
		Session session = sessionFactory.getCurrentSession();
		try {
			Book book = new Book();
			book.setIdBook(idBook);
			session.delete(book);
			System.out.println("delete OK");
			return 1;
		} catch (Exception e) {
			System.out.println("delete fail");
			return 0;
		}
	}

	@Override
	public int updateBook(Book book) {
		Session session = sessionFactory.getCurrentSession();
		 SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm, dd-MM-yyyy");//dd/MM/yyyy
		    Date now = new Date();
		    String formattedDate = sdfDate.format(now);
		try {
			Book book2 = (Book) session.createQuery("from Book where idBook = "+book.getIdBook()+"").getSingleResult();
			book2.setBookName(book.getBookName());
			book2.setImg(book.getImg());
			book2.setContent(book.getContent());
			book2.setCreateAt(formattedDate);
			session.update(book2);
			return 1;
			
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public int changeStaus(int idBook,int status) {
		
		Session session = sessionFactory.getCurrentSession();
		try {
			Book book = (Book) session.createQuery("from Book where idBook = "+idBook+"").getSingleResult();
			book.setStatus(status);
			session.update(book);
			return 1;
			
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public int Addlog(Audit_log audit_log) {
		Session session = sessionFactory.getCurrentSession();
		session.save(audit_log);
		return 0;
	}
	@Override
	public List<Book> search(int index, String value) {
		Session session = sessionFactory.getCurrentSession();
		if(value == "")
		{
			List<Book> listBooks = session.createQuery("from Book").getResultList();
			return listBooks;
		}
		if(index == 0){
			List<Book> listBooks = session.createQuery("from Book where BookName LIKE '%"+value+"%'").getResultList();
			return listBooks;
		}else if (index == 1) {
			List<Book> listBooks = session.createQuery("from Book where authors LIKE '%"+value+"%'").getResultList();
			return listBooks;
		}else {
			List<Book> listBooks = session.createQuery("from Book where Category LIKE '%"+value+"%'").getResultList();
			return listBooks;
		}
	}

	@Override
	public List<Book> getType(int IdType) {
		Session session = sessionFactory.getCurrentSession();
		if(IdType == 0)
		{
			List<Book> listBooks = session.createQuery("from Book where Category LIKE '%programming%'").getResultList();
			return listBooks;
		}else if(IdType == 1){
			List<Book> listBooks = session.createQuery("from Book where Category LIKE '%psychology%'").getResultList();
			return listBooks;
		}else if (IdType == 2) {
			List<Book> listBooks = session.createQuery("from Book where Category LIKE '%biological%'").getResultList();
			return listBooks;	
		}else if (IdType == 3) {
			List<Book> listBooks = session.createQuery("from Book where Category LIKE '%textbook%'").getResultList();
			return listBooks;	
		}else {
			List<Book> listBooks = session.createQuery("from Book where Category LIKE '%literary%'").getResultList();
			return listBooks;
		}
		
	}

	@Override
	public Book showdetail(int id) {
		Session session = sessionFactory.getCurrentSession();
		Book listBooks = (Book) session.createQuery("from Book where idBook ="+id+"").getSingleResult();
		return listBooks;
	}

	@Override
	public List<Audit_log> showFilelog() {
		Session session = sessionFactory.getCurrentSession();
		List<Audit_log> Lislog = session.createQuery("from Audit_log order by id DESC").getResultList();
		return Lislog;
	}
	
	
}
