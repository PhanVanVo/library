package library.pvv.DaoImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import library.pvv.dao.commentDao;
import library.pvv.model.Book;
import library.pvv.model.BookUser;
import library.pvv.model.comment;

@Repository
@Transactional
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class commentDaoImpl implements commentDao {
	@Autowired
	SessionFactory sessionFactory;

	//add comment
	@Override
	public int addcmt(int iduser, int idbook, String content, int rankStart) {
		Session session = sessionFactory.getCurrentSession();

		comment comment = new comment();
		SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm - dd-MM-yyyy");//dd/MM/yyyy
	    Date now = new Date();
	    String formattedDate = sdfDate.format(now);	

		try {
			Book book = (Book) session.createQuery("from Book where idBook  = " + idbook + "").getSingleResult();
			comment.setBook(book);
			BookUser User = (BookUser) session.createQuery("from BookUser where idBookUser = " + iduser + "").getSingleResult();
			comment.setBookUser(User);
			comment.setContent(content);
			comment.setRankStart(rankStart);
			comment.setTime(formattedDate);
			session.save(comment);
			return 1;
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public List<comment> showCMT(int idbook) {
		Session session = sessionFactory.getCurrentSession();
		try {
			List<comment> listcomment = session.createQuery("from comment where idBook ="+idbook+"").getResultList();
			return listcomment;
		} catch (Exception e) {
			return null;
		}
	}
	
	

}
