package library.pvv.DaoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import library.pvv.dao.BookUserDao;
import library.pvv.model.BookUser;

@Repository
@Transactional
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BookUserDaoImpl implements BookUserDao {

	

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<BookUser> selectUser() {
		// TODO Auto-generated method stub
		return null;
	}
	//registered user
	@Override
	public int addUser(BookUser bookUser) {
		Session session = sessionFactory.getCurrentSession();
		try {
			//Check if the user exists
			BookUser bookUser1 = (BookUser) session.createQuery("from BookUser where username = '" + bookUser.getUsername() + "'").getSingleResult();
			if(bookUser1 != null) {
				return 0;
			}			
		} 
		catch (Exception e) {
			session.save(bookUser);
			return 1;
		}

		return 0;
	}
	@Override
	public boolean checkLogin(String username, String pass) {
		Session session = sessionFactory.getCurrentSession();
		try {
			BookUser User = (BookUser) session.createQuery("from BookUser where username ='"+ username +"' and password ='"+pass+"'" ).getSingleResult();
			if (User != null)
			{
				System.out.println("login  thanh cong");
				return true;
			}
		} catch (Exception e) {
			System.out.println("login khong thanh cong");
			return false;
		}
		System.out.println("login khong thanh cong");
		return false;
	}

	@Override
	public BookUser showInfor(String username) {
		Session session = sessionFactory.getCurrentSession();
		try {
			BookUser bookUser = (BookUser) session.createQuery("from BookUser where username ='"+ username +"'" ).getSingleResult();
			return bookUser;
		} catch (Exception e) {
			return null;
		}
		
	}

	
	

}
