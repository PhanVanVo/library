package library.pvv.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.tomcat.jni.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import library.pvv.DaoImpl.commentDaoImpl;
import library.pvv.model.Book;
import library.pvv.model.BookUser;
import library.pvv.model.comment;
import library.pvv.serviceimpl.BooksServiceImpl;
import library.pvv.serviceimpl.commentServiceImpl;

@Controller
@RequestMapping("member")
// get session use page jsp of controller
@SessionAttributes({ "username", "rool","user"})
public class controllerMem {
	@Autowired
	BooksServiceImpl booksServiceImpl;
	@Autowired
	commentServiceImpl commentServiceImpl;
	SessionFactory sessionFactory;
	
	private static final Logger log = Logger.getLogger(controllerMem.class);
	
	@GetMapping
	public String Default(ModelMap modelMap, HttpSession session)
	{
		
			List<Book> listBook = booksServiceImpl.showListBooks();
			modelMap.addAttribute("listBook",listBook);
				return "member";
	}

	// index: 0: search by Book name, 1: search by authors, 2: search by Category.
	// value: Content to search
	//method returns json to client for processing 
	@RequestMapping(value = "/search")
	public @ResponseBody List<Book> search(@RequestParam String index,@RequestParam String value, HttpServletResponse response, ModelMap model,
			HttpSession session) {
		 List<Book> dssp =new ArrayList<>();
        if(value==null || value.equals("")){
        	  
        	   dssp = booksServiceImpl.showListBooks();
        	   // if value = "" return All Book
        }else{
        	   System.out.println("ma loai sp: "+value);
        	   int IdType=Integer.parseInt(index);
        	   dssp = booksServiceImpl.search(IdType, value);
        	   System.out.println(dssp.get(0).getBookName());
        	   model.addAttribute("dssp", dssp);  
        }
		return dssp;
	}
	
	//sorted by book type, use Json same method search.
	@RequestMapping(value = "/typebook")
	public @ResponseBody List<Book> typebook(@RequestParam String maloaisp, HttpServletResponse response, ModelMap model,
			HttpSession session) {
		 List<Book> dssp =new ArrayList<>();
        if(maloaisp==null || maloaisp.equals("")){
        	   System.out.println("not get ID Type");	
        }else{
        	   System.out.println("ma loai sp: "+maloaisp);
        	   int IdType=Integer.parseInt(maloaisp);
        	   dssp = booksServiceImpl.getType(IdType);
        	   System.out.println(dssp.get(0).getBookName());
        	   model.addAttribute("dssp", dssp);  
        }
		return dssp;
	}
	
	//comment and rate any book 
	@RequestMapping(value = "/comment")
	public String comment(@RequestParam String detailid,@RequestParam String startRank, @RequestParam String content, HttpServletResponse response, ModelMap model,
			HttpSession session) {
		System.out.println("comment");
		 comment dssp = new comment();
		if (content == null || content.equals("")) {
			System.out.println("ko lay dc maloaisp");
			return "detail";
			}else {
				BookUser user =(BookUser) session.getAttribute("user");
				int start=Integer.parseInt(startRank);
				int idbook=Integer.parseInt(detailid);
				int resul =  commentServiceImpl.addcmt(user.getIdBookUer(),idbook,content,start);
				System.out.println("Ket qua = " + resul);
				return "detail";
		}
	}
	
	// show detail book (see commnet and comment)
	 @RequestMapping("/detail")
	    public String showProductDetail(HttpServletRequest request, HttpServletResponse response, Model model,
	                                    HttpSession session) {
	        int id = Integer.parseInt(request.getParameter("id"));
	        Book bookdetail = booksServiceImpl.showdetail(id);
	        model.addAttribute("bookdetail", bookdetail);
	        List<comment> listcmt = commentServiceImpl.showCMT(id);
	        model.addAttribute("listcmt", listcmt);
	        float start = 0, temp = 0;
	        for (comment cmt : listcmt) {
				System.out.println(cmt.getBookUser().getRooller());
				System.out.println(cmt.getContent());
				System.out.println(cmt.getRankStart());
				System.out.println(cmt.getTime());
				System.out.println(cmt.getBook().getImg());
				System.out.println(cmt.getRankStart());
				start += cmt.getRankStart();
				temp += 1;
			}
	        start = start/temp;
	        model.addAttribute("start", start);
	        return "detail";
	    }
	 
}
