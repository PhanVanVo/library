package library.pvv.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import library.pvv.model.Audit_log;
import library.pvv.model.Book;
import library.pvv.serviceimpl.BooksServiceImpl;

@Controller
@RequestMapping("CRUDBook")
@SessionAttributes({ "username", "rool","user"})
public class ControllerBooks {
	private static final Logger log = Logger.getLogger(ControllerBooks.class);
	SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm - dd-MM-yyyy");//dd/MM/yyyy
    Date now = new Date();
    String formattedDate = sdfDate.format(now);
	@Autowired
	BooksServiceImpl booksServiceImpl;
	@Autowired
	SessionFactory sessionFactory;
	
	
	// method Default of site /Library/CRUDBook
	// show with only admin
	// if member go to here. it return /Library/member
	// If not already logged in, you will be redirected to the login page
	@GetMapping
	public String Default(ModelMap modelMap, HttpSession session)
	{
		try {
			int rol = (int) session.getAttribute("rool");
			String usernameString = session.getAttribute("username").toString();
			System.out.println("rol = " + rol);
			List<Book> listBook = booksServiceImpl.showListBooks();
			modelMap.addAttribute("listBook",listBook);
			// if roll == 1 that admin, else member
			if (rol == 1){
				
				List<Audit_log> listLogs = booksServiceImpl.showFilelog();
				modelMap.addAttribute("listLogs",listLogs);
				
				log.info("admin "+ usernameString + " have User login");
				return "CRUDBook";
			}else{
				log.info("Member "+ usernameString + " have User login");
				return "redirect:member";
			}
		} catch (Exception e) {
			log.error("have person unauthorized access");
				return "redirect:login";

		}		
	}

	@Autowired
	ServletContext context; 
	
	// file: file book ( file text *pdf, *txt,  ..... )
	// avatar: this is book cover ( file img)
	// The bookN, authors, category parameters and file, content are taken from the file CRUDBook.JSP
	@RequestMapping("/createBook")
	@PostMapping
	public String createBook(@RequestParam("BookN") String BookN,@RequestParam("authors") String authors,@RequestParam("category") String category,@RequestParam("avatar") MultipartFile avatar,@RequestParam("file") MultipartFile file,
			ModelMap modelMap,HttpSession session){
		 String status="";
		 String img ="null", content= "null";
		 	String pathImgString = "/resources/img";
		 	String pathContentString = "/resources/bookFile";
		 	img = saveFile(pathImgString, avatar);
		 	System.out.println( img);
		 	content = saveFile(pathContentString, file);
		 	System.out.println( content);
			if (BookN.equals(""))
			{
				BookN = formattedDate;
			}
			
			Book book = new Book();
			book.setBookName(BookN);
			book.setImg(img);
			book.setContent(content);
			book.setCreateAt(formattedDate);
			book.setStatus(1);
			book.setAuthors(authors);
			book.setCategory(category);
			int result = booksServiceImpl.AddBook(book);
			System.out.println("1111111111111");
			return "CRUDBook";
	}
	
	// path: Address to save the file wich you want
	// file: file to save
	// The function returns the OriginalFilename
	private String saveFile(String path, MultipartFile file) {
		String status="";
		String linkfile= "null";
		 if (!file.isEmpty()) {
	            try {
	            byte[] bytes = file.getBytes();
	            String path_fileString = context.getRealPath(path);
	            File dir = new File(path_fileString);   
	            if (!dir.exists())
	                    dir.mkdirs();
	                File uploadFile = new File(dir.getAbsolutePath()+ File.separator + 
	                                         file.getOriginalFilename());
	                BufferedOutputStream outputStream = new BufferedOutputStream(
	                                new FileOutputStream(uploadFile));
	                outputStream.write(bytes);
	                outputStream.close();
	                status = "";
	                linkfile = file.getOriginalFilename();
	                System.out.println(linkfile);
	                status = status +  " Successfully uploaded file=" + file.getOriginalFilename();
	                System.out.println(status);
	            } catch (Exception e) {
	                status = status +  "Failed to upload " + file.getOriginalFilename()+ " " + e.getMessage();
	            }
	        }
		return linkfile;
	}
	
	// delete book in database
	@RequestMapping("/deleteBook")
	@PostMapping
	public void deletebook(@RequestParam int idBook, ModelMap modelMap) {
		
		System.out.println("adsfdgfhjg" + idBook);
			int result = booksServiceImpl.deleteBook(idBook);
			System.out.println("kkkkkkkkkkk");
			

	}
	
	//-----hide/show books with member------
	
	@RequestMapping("/ChangeStatus")
	@PostMapping
	public void ChangeStatus(@RequestParam int idBook, @RequestParam int status,ModelMap modelMap) {
			System.out.println(status + "-" +idBook);
			int result = booksServiceImpl.changeStaus(idBook,status);
			System.out.println("hii");	
		
}
	}
