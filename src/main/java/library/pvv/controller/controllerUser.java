package library.pvv.controller;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import library.pvv.model.Audit_log;
import library.pvv.model.BookUser;
import library.pvv.serviceimpl.BooksServiceImpl;
import library.pvv.serviceimpl.BooksUserServiceimpl;
import library.pvv.utils.Encryption;
@Controller
@RequestMapping("/")
public class controllerUser {
	private static final Logger log = Logger.getLogger(controllerUser.class);

	SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm - dd-MM-yyyy");//dd/MM/yyyy
    Date now = new Date();
    String formattedDate = sdfDate.format(now);	
	@Autowired
	BooksUserServiceimpl booksUserServiceimpl;
	@Autowired
	BooksServiceImpl booksServiceImpl;
	@Autowired
	SessionFactory sessionFactory;

	//page index
	@GetMapping("/")
	public String home()
	{
		return "home";
	}
	
	@GetMapping("register")
	public String registerDefault() {
	    return "register"; 
	}
	
	//method register
	// if password = "phanvanvo" this is admin if the other is a member
	//after register success move to page login if fail still here
	@PostMapping("register")
	@Transactional
	public String register(@RequestParam String fullname, @RequestParam String username, 
			@RequestParam String psw, ModelMap modelMap, HttpSession session) {
		BookUser user = new BookUser();
		user.setFullname(fullname);
		user.setUsername(username);
		user.setPassword(Encryption.encrypt(psw));
		int rl = 0;
		if ("phanvanvo".equals(psw))
		{
			 rl = 1;
		}
		user.setRooller(rl);
		int result = booksUserServiceimpl.addUser(user);
		if (result == 1) {
			System.out.println("register success");
			try {
				String usernameString = session.getAttribute("username").toString();
				Session session1 = sessionFactory.getCurrentSession();
				//audit_log save file log in to database
				Audit_log audit_log = new Audit_log();
				BookUser user1 = (BookUser) session1.createQuery("from BookUser where username = '"+ usernameString +"'").getSingleResult();
				audit_log.setTimeLine(formattedDate);
				audit_log.setEvent("persion register");
				String rool = "member";
				if(user1.getRooller() == 1){
					rool = "admin";
				}
				audit_log.setDescription(rool + " '"+fullname + "' created account with usrename = '" + username+"'");
				audit_log.setBookUser(user);
				log.info(user1.getFullname());
				int inst = booksServiceImpl.Addlog(audit_log);
			} catch (Exception e) {
				System.out.println("fail Addlog out");
			}
			return "redirect:login";
		} else {
			System.out.println("register fail");
			return "register";
		}

	}
	
	@GetMapping("login")
	public String loginDefault() {
		return "login";
	}

	// if admin login redirect to page CRUDBook
	// if member login redirect to page member
	@PostMapping("login")
	@Transactional
	public String login(@RequestParam String username, @RequestParam String psw, ModelMap Model,HttpServletRequest request) {
		
		HttpSession session = request.getSession(true);
		BookUser user = booksUserServiceimpl.showInfor(username);
		
		System.out.println(username);
		 
		session.setAttribute("user", user);
		session.setAttribute("username", username);
		session.setAttribute("rool", user.getRooller());
		
		
		
		
		boolean inforIsCorrect = booksUserServiceimpl.checkLogin(username, Encryption.encrypt(psw));
		log.info(user.getFullname() + " login web");
		
		if(inforIsCorrect)
		{
			log.debug(user.getFullname() + " login web");
			try {
				
				Audit_log audit_log = new Audit_log();
				audit_log.setTimeLine(formattedDate);
				audit_log.setEvent("User Login");
				
				String rool = "member";
				if(user.getRooller() == 1){
					rool = "admin";
				}
				audit_log.setDescription("user " + user.getFullname() + "this is " + rool);
				audit_log.setBookUser(user);
				log.info(user.getFullname());
				int inst = booksServiceImpl.Addlog(audit_log);
			} catch (Exception e) {
				System.out.println("fail Addlog login");
			}
			System.out.println(user.toString());
			if(user.getRooller() == 1)
			{
				return "redirect:CRUDBook";
			}
			else {
				return "redirect:member";
			}
		}
		else {
			return "login";
		}
	}
	
	@GetMapping("/logout")
	@Transactional
	public String Default(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.invalidate();
		Cookie cookie = new Cookie("JSESSIONID", null);
		cookie.setMaxAge(0);
		response.addCookie(cookie);
		try {
			
			String usernameString = session.getAttribute("username").toString();
			
			Session session1 = sessionFactory.getCurrentSession();
			Audit_log audit_log = new Audit_log();
			BookUser user = (BookUser) session1.createQuery("from BookUser where username = '"+ usernameString +"'").getSingleResult();
			audit_log.setTimeLine(formattedDate);
			audit_log.setEvent("User Logout");
			String rool = "member";
			if(user.getRooller() == 1){
				rool = "admin";
			}
			audit_log.setDescription("user " + user.getFullname() + "this is " + rool);
			audit_log.setBookUser(user);
			log.info(user.getFullname());
			int inst = booksServiceImpl.Addlog(audit_log);
		} catch (Exception e) {
			System.out.println("fail Addlog out");
		}
		
		System.out.println("Logout thanh cong");
		 return "redirect:/";
	}

}