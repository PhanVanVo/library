package library.pvv.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import library.pvv.DaoImpl.BookUserDaoImpl;
import library.pvv.model.BookUser;
import library.pvv.service.BookUserService;
@Service
public class BooksUserServiceimpl implements BookUserService{
	@Autowired
	BookUserDaoImpl bookUserDaoImpl;

	@Override
	public int addUser(BookUser bookUser) {
		return bookUserDaoImpl.addUser(bookUser);
	}

	@Override
	public boolean checkLogin(String username, String pass){
		return bookUserDaoImpl.checkLogin(username, pass);
	}

	@Override
	public BookUser showInfor(String username) {
		// TODO Auto-generated method stub
		return bookUserDaoImpl.showInfor(username);
	}
	
	
}
