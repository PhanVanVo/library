package library.pvv.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import library.pvv.DaoImpl.BooksDaoImpl;
import library.pvv.model.Audit_log;
import library.pvv.model.Book;
import library.pvv.service.BooksService;
@Service
public class BooksServiceImpl implements BooksService{
	@Autowired
	BooksDaoImpl booksDaoImpl;

	@Override
	public List<Book> showListBooks() {
		// TODO Auto-generated method stub
		return booksDaoImpl.showListBooks();
	}

	@Override
	public int AddBook(Book book) {
		// TODO Auto-generated method stub
		return booksDaoImpl.AddBook(book);
	}

	@Override
	public int deleteBook(int idBook) {
		// TODO Auto-generated method stub
		return booksDaoImpl.deleteBook(idBook);
	}

	@Override
	public int updateBook(Book book) {
		// TODO Auto-generated method stub
		return booksDaoImpl.updateBook(book);
	}

	@Override
	public int changeStaus(int idBook,int status) {
		// TODO Auto-generated method stub
		return booksDaoImpl.changeStaus(idBook,status);
	}

	@Override
	public int Addlog(Audit_log audit_log) {
		// TODO Auto-generated method stub
		return booksDaoImpl.Addlog(audit_log);
	}

	@Override
	public List<Book> search(int index, String valuestrString) {
		// TODO Auto-generated method stub
		return booksDaoImpl.search(index, valuestrString);
	}

	@Override
	public List<Book> getType(int IdType) {
		// TODO Auto-generated method stub
		return booksDaoImpl.getType(IdType);
	}

	@Override
	public Book showdetail(int id) {
		// TODO Auto-generated method stub
		return booksDaoImpl.showdetail(id);
	}

	@Override
	public List<Audit_log> showFilelog() {
		// TODO Auto-generated method stub
		return booksDaoImpl.showFilelog();
	}
	
	
}
