package library.pvv.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import library.pvv.DaoImpl.commentDaoImpl;
import library.pvv.model.comment;
import library.pvv.service.commentService;
@Service
public class commentServiceImpl implements commentService{
	@Autowired
	commentDaoImpl commentDaoImpl;
	@Override
	public int addcmt(int iduser, int idbook, String content, int rankStart) {
		// TODO Auto-generated method stub
		return commentDaoImpl.addcmt(iduser, idbook,content, rankStart);
	}
	@Override
	public List<comment> showCMT(int idbook) {
		// TODO Auto-generated method stub
		return commentDaoImpl.showCMT(idbook);
	}
	
	
}
